﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Practica1
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            AldoLima v = new AldoLima();
            v.Show();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            AxelMartinez v = new AxelMartinez();
            v.Show();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            BrandonMontoya v = new BrandonMontoya();
            v.Show();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            EdwinCrespo v = new EdwinCrespo();
            v.Show();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            JhasmaniMartinez v = new JhasmaniMartinez();
            v.Show();
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            JhonnShorts v = new JhonnShorts();
            v.Show();
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            JimenaGozales v = new JimenaGozales();
            v.Show();
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            JoaquinJimenez v = new JoaquinJimenez();
            v.Show();
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            JonathaHerbas v = new JonathaHerbas();
            v.Show();
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            JoseColque v = new JoseColque();
            v.Show();
        }

        private void Button_Click_10(object sender, RoutedEventArgs e)
        {
            JoseBascope v = new JoseBascope();
            v.Show();
        }

        private void Button_Click_11(object sender, RoutedEventArgs e)
        {
            JoseQuiroga v = new JoseQuiroga();
            v.Show();
        }

        private void Button_Click_12(object sender, RoutedEventArgs e)
        {
            LeonelCalderon v = new LeonelCalderon();
            v.Show();
        }

        private void Button_Click_13(object sender, RoutedEventArgs e)
        {
            MiguelTerrazas v = new MiguelTerrazas();
            v.Show();
        }

        private void Button_Click_14(object sender, RoutedEventArgs e)
        {
            MilenkaGarcia v = new MilenkaGarcia();
            v.Show();
        }

        private void Button_Click_15(object sender, RoutedEventArgs e)
        {
            MirkoLazarte v = new MirkoLazarte();
            v.Show();
        }

        private void Button_Click_16(object sender, RoutedEventArgs e)
        {
            RodrigoIriarte v = new RodrigoIriarte();
            v.Show();
        }

        private void Button_Click_17(object sender, RoutedEventArgs e)
        {
            SebastianClavijo v = new SebastianClavijo();
            v.Show();
        }

        private void Button_Click_18(object sender, RoutedEventArgs e)
        {
            YamilBakry v = new YamilBakry();
            v.Show();
        }

        private void Button_Click_19(object sender, RoutedEventArgs e)
        {
            AlexanderPerez v = new AlexanderPerez();
            v.Show();
        }

        private void Button_Click_20(object sender, RoutedEventArgs e)
        {
            ErlanVedia v = new ErlanVedia();
            v.Show();
        }

        private void Button_Click_21(object sender, RoutedEventArgs e)
        {
            JhonatanSanga v = new JhonatanSanga();
            v.Show();
        }

        private void Button_Click_22(object sender, RoutedEventArgs e)
        {
            KevinBautista v = new KevinBautista();
            v.Show();
        }

        private void Button_Click_23(object sender, RoutedEventArgs e)
        {
            MichaelCoca v = new MichaelCoca();
            v.Show();
        }

        private void Button_Click_24(object sender, RoutedEventArgs e)
        {
            MirkoMarca v = new MirkoMarca();
            v.Show();
        }

        private void Button_Click_25(object sender, RoutedEventArgs e)
        {
            PabloRodriguez v = new PabloRodriguez();
            v.Show();
        }

        private void Button_Click_26(object sender, RoutedEventArgs e)
        {
            FernandoAlmaraz v = new FernandoAlmaraz();
            v.Show();
        }

        private void Button_Click_27(object sender, RoutedEventArgs e)
        {
            JoanCastro v = new JoanCastro();
            v.Show();
        }

        private void Button_Click_28(object sender, RoutedEventArgs e)
        {
            PaolaGonzales v = new PaolaGonzales();
            v.Show();
        }

        private void Button_Click_29(object sender, RoutedEventArgs e)
        {
            PeterCalcina v = new PeterCalcina();
            v.Show();
        }
    }
}
